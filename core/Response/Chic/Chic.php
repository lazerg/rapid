<?php

namespace Core\Response\Chic;

class Chic
{
    private array $directives = [
        'asset' => \Core\Response\Chic\Directives\AssetDirective::class,
    ];

    public function __construct() {}

    public function addDirective(string $name, string $directive): void
    {
        $this->directives[$name] = $directive;
    }

    public function getDirective(string $name): string
    {
        return $this->directives[$name];
    }
}