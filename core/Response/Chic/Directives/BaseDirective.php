<?php

namespace Core\Response\Chic\Directives;

abstract class BaseDirective
{
    public function __construct(){}

    protected bool $isEcho = true;

    protected function wrap(string $code): string
    {
        return php($code, $this->isEcho);
    }

    public function render(string $variable): string
    {
        return 'Component render function is not written';
    }
}