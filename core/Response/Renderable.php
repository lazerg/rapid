<?php

namespace Core\Response;

interface Renderable
{
    public function render(): void;
}