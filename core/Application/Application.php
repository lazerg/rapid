<?php

namespace Core\Application;

use Core\Config\Config;
use Core\Request\Request;
use Core\Response\Chic\Chic;
use Core\Router\Routing;

class Application
{
    public static Application $app;

    public Config $config;
    public Request $request;
    public Routing $router;
    public Chic $chic;

    public function __construct()
    {
        self::$app = $this;
        $this->config = new Config();
        $this->request = new Request();
        $this->router = new Routing();
        include ROOT_PATH . 'app/routes.php';
        $this->chic = new Chic();

        $this
            ->router
            ->matchRoute()
            ->resolveRoute($this->request)
            ->render();
    }
}