<?php

namespace Core\Response;

class BaseResponse
{
    protected array $data = [];
    protected int $statusCode = 200;

    public function __construct($data = [], $status = 200)
    {
        $this->data = $data;
        $this->setStatusCode($status);
    }

    public function setStatusCode(int $statusCode)
    {
        http_response_code($statusCode);
        $this->statusCode = $statusCode;
    }

    public function __set(string $name, $value): void
    {
        $this->{$name} = $value;
    }
}