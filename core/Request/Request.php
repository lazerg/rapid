<?php

namespace Core\Request;

class Request
{
    public string $url;
    public string $method;
    private array $data;

    public function __construct()
    {
        $this->url = $this->analyzeUrl();
        $this->method = $this->analyzeMethod();
        $this->data = $this->analyzeData();
    }

    private function analyzeUrl(): string
    {
        return '/' . trim($_SERVER['PATH_INFO'] ?? '/', '/');
    }

    private function analyzeMethod(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    private function analyzeData(): array
    {
        return array_map(fn($val) => filter_var($val, 522), array_collapse([$_GET, $_POST]));
    }

    public function get(string $key, mixed $default = null): mixed
    {
        return array_get($this->data, $key, $default);
    }

    public function only(array|string $keys): array
    {
        return array_only($this->data, $keys);
    }

    public function all(): array
    {
        return $this->data;
    }
}