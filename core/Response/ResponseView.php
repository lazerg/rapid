<?php

namespace Core\Response;

/** @property string $path */
class ResponseView extends BaseResponse implements Renderable
{
    public function render(): void
    {
        $template = new \Core\Response\Chic\TemplateEngine($this->path);

        foreach ($this->data as $k => $v) $$k = $v;

        /** @noinspection PhpIncludeInspection */
        include_once $template->save();
    }
}