<?php

use App\Controllers\MainController;
use Core\Router\Router;

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Here is where you can register routes for your application.
| Now create something great!
|
*/

Router::get('/', [MainController::class, 'index']);

Router::get('/users/{user_id}', function (\Core\Request\Request $request, $user_id) {
    $content = file_get_contents('https://jsonplaceholder.typicode.com/users/' . $user_id);
    $user = json_decode($content, true);

    return \Core\Response\Response::json($user);
});