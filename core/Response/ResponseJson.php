<?php

namespace Core\Response;

class ResponseJson extends BaseResponse implements Renderable
{
    public function render(): void
    {
        header('Content-Type: application/json');
        echo json_encode($this->data);
    }
}