<?php

namespace Core\Config;

class Config
{
    private array $data = [];

    public function __construct()
    {
        $dotenv = \Dotenv\Dotenv::createImmutable(ROOT_PATH);
        $dotenv->load();

        $this->initializeConfigs();
    }

    private function initializeConfigs()
    {
        foreach (glob(ROOT_PATH . 'app/Config/*.php') as $file) {
            array_set($this->data, pathinfo($file, PATHINFO_FILENAME), include $file);
        }
    }

    public function config(string $key, mixed $default = '')
    {
        return array_get($this->data, $key, $default);
    }
}