<?php

namespace Core\Response\Chic;

use Core\Application\Application;
use Core\Response\Chic\Directives\BaseDirective;

class TemplateEngine
{
    /**
     * Path to view file on `app/Views/`
     *
     * @var string $realPath
     */
    private string $realPath;

    /**
     * Path to rendered template file on `app/Cache/Views/`
     *
     * @var string
     */
    private string $hashPath;

    /**
     * Current state of rendered template
     *
     * @var string $template
     */
    private string $template;

    public function __construct(string $path)
    {
        $this->hashPath = ROOT_PATH . 'app/Cache/Views/' . md5($path) . '.php';
        $this->realPath = ROOT_PATH . 'app/Views/' . str_replace('.', '/', $path) . '.chic.php';

        $this
            ->retrieveTemplate()
            ->renderDirectives()
            ->renderDoubleScopedExpressions()
            ->renderStats();
    }

    /**
     * Get contents of view and save it into $template
     *
     * @return $this
     */
    private function retrieveTemplate(): static
    {
        ob_start();
        /** @noinspection PhpIncludeInspection */
        include_once $this->realPath;
        $this->template = ob_get_clean();
        return $this;
    }

    /**
     * Render directives
     *
     * @return $this
     * @example @asset('rapid.png')
     */
    private function renderDirectives(): static
    {
        $REGEX = "/@([A-z]*)\(([A-za-z'$.]*)\)/";

        $this->replace($REGEX, function ($matches) {
            /** @type BaseDirective $directive */
            $directive = new (Application::$app->chic->getDirective($matches[1]))();

            return $directive->render($matches[2]);
        });

        return $this;
    }

    /**
     * Render double scoped expressions
     *
     * @return $this
     */
    private function renderDoubleScopedExpressions(): static
    {
        $REGEX = "/{{ ?([^}]*) ?}}/";

        $this->replace($REGEX, fn($matches) => php($matches[1]));

        return $this;
    }

    /**
     * Render stats on bottom of rendered template
     *
     * @return void
     */
    private function renderStats(): void
    {
        $this->template .= PHP_EOL . php('//' . $this->realPath, false);
    }

    /**
     * Replace template by given regular expression pattern
     *
     * @param string $regex
     * @param callable $callback
     */
    private function replace(string $regex, callable $callback): void
    {
        $this->template = (string)preg_replace_callback($regex, $callback, $this->template);
    }

    /**
     * Save rendered template and return path to it
     *
     * @return string
     */
    public function save(): string
    {
        file_put_contents($this->hashPath, $this->template);
        return $this->hashPath;
    }
}