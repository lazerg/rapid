<?php

namespace Core\Response\Chic\Directives;

class AssetDirective extends BaseDirective
{
    public function render(string $variable): string
    {
        return $this->wrap("assets($variable)");
    }
}