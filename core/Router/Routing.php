<?php

namespace Core\Router;

use Core\Request\Request;
use Core\Response\Response;
use Core\Response\Renderable;

class Routing
{
    private array $routes = [];
    private array $currentRoute = [];

    const PARAM_REGEX_REPLACE = '/{([A-z]*)}/';
    const PARAM_REGEX = '(?P<%s>.*)';

    public function __construct()
    {

    }

    public function addRoute($method, $url, $action)
    {
        $this->routes[$method][] = [
            'pattern' => '/' . trim($url, '/'),
            'action' => $action,
        ];
    }

    public function matchRoute(): static
    {
        foreach ($this->routes[request()->method] as $route) {
            $pattern = preg_replace_callback(self::PARAM_REGEX_REPLACE, fn($match) => sprintf(self::PARAM_REGEX, $match[1]), $route['pattern']);
            $pattern = sprintf('/^%s$/', str_replace('/', '\/', $pattern));

            preg_match($pattern, request()->url, $matches);

            if (count($matches) !== 0) {
                $matches = array_filter($matches, fn($key) => !is_numeric($key), ARRAY_FILTER_USE_KEY);

                $this->currentRoute = [
                    'matches' => $matches,
                    'action' => $route['action'],
                ];

                break;
            }
        }

        return $this;
    }

    public function resolveRoute(Request $request): Renderable
    {
        if ($this->currentRoute === []) {
            return Response::view('default._404');
        }

        $arguments = [
            $request,
            ...array_values($this->currentRoute['matches'])
        ];

        if (is_callable($this->currentRoute['action'])) {
            return call_user_func($this->currentRoute['action'], ...$arguments);
        }

        if (is_array($this->currentRoute['action'])) {
            [$controller, $action] = $this->currentRoute['action'];
            return call_user_func_array([new $controller, $action], $arguments);
        }
    }
}