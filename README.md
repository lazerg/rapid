## Installation

- Clone framework repository to your computer `git clone https://gitlab.com/lazerg/rapid && cd rapid`;
- Install necessary dependencies `composer install`;
- Make copy of local environmental variables `cp .env.example .env`;
- Run a project `php -S localhost:8000 -t public`;

## Routing

The most basic Rapid routes accept a URI and a closure, providing a very simple and expressive method of defining routes and behavior without complicated routing configuration files:
```
Router::get('/assert', function(\Core\Request\Request $request){
    return Response::json([
        'success' => 'true'
    ]);
});
```

For most applications, you will begin by defining routes in your `app/routes.php` file. 
The routes defined in `app/routes.php` may be accessed by entering the defined route's URL in your browser. 
For example, you may access the following route by navigating to `http://example.com/contact` in your browser:

```
Router::get('/contact', [ContactFormController::class, 'show']);
```

Sometimes you will need to capture segments of the URI within your route. 
For example, you may need to capture a user's ID from the URL. You may do so by defining route parameters:

```
Router::get('/users/{id}', function (Request $request, $id) {
    return Response::view('user', [
        'name'    => 'Lazizbek',
        'surname' => 'Ergashev',
    ]);
});
```

You may define as many route parameters as required by your route:

```
Route::get('/posts/{post}/comments/{comment}', function (Request $request, $postId, $commentId) {
    //
});
```

## Request instance

Request class provides an object-oriented way to interact with the current HTTP request being handled by your application as well as retrieve the input, ~~cookies~~, and ~~files~~ that were submitted with the request.
Rapid framework also provides `request()` helper to manipulate with Request easily

- `request()->get('name');`
- `request()->only(['name', 'surname']);`

## Responding

Of course, it's not practical to return entire HTML documents strings directly from your routes and controllers. 
Thankfully, views provide a convenient way to place all of our HTML in separate files. 
Views separate your controller / application logic from your presentation logic and are stored in the `app/Views` directory. 
A simple view might look something like this:

```
<!-- View stored in app/Views/main_page.php -->

<html>
    <body>
        <h1>Hello, <?=$name?></h1>
    </body>
</html>
```

To show this view, we should return this view in controller

```
public function index(){
    $name = 'Lazizbek';
    return Response::view('main_page', compact('name'));
}
```

**Or you can respond as json format**

```
return Response::json([
    'name'    => 'Lazizbek'
    'surname' => 'Ergashev'
]);
```

## View
**Chic** is the simple, yet powerful templating engine that is included with Rapid Framework.
Unlike some PHP templating engines, Chic does not restrict you from using plain PHP code in your templates. 

In fact, all Chic templates are compiled into plain PHP code and cached until they are modified, 
meaning Chic adds essentially zero overhead to your application. 

Chic template files use the `.chic.php` file extension and are typically stored in the `app/Views/` directory.

- `{{ $_SERVER['SERVER_SOFTWARE'] }}` —> `<?php echo $_SERVER['SERVER_SOFTWARE']; ?>`;
- `{{ 30+50 }}` —> 80;

```
// create file app/Views/dashboard/main_page.chic.php

return Response::view('dashboard.main_page', [
    'name' => 'Lazizbek',
]);

// inside of view

<div>
    Good morning, {{ $name }} <!-- Good morning, Lazizbek -->
</div>
```