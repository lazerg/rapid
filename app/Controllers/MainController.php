<?php

namespace App\Controllers;

use Core\Request\Request;
use Core\Response\Response;
use Core\Response\ResponseView;

class MainController
{
    public function index(Request $request): ResponseView
    {
        return Response::view('index_page', [
            'year' => date("Y"),
        ]);
    }
}