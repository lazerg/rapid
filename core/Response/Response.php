<?php

namespace Core\Response;

class Response
{
    public static function view($path, $data = [], int $status = 200)
    {
        $view = new ResponseView($data, $status);
        $view->path = $path;
        return $view;
    }

    public static function json($data = [], int $status = 200)
    {
        return new ResponseJson($data, $status);
    }
}