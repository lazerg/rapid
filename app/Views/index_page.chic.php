<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rapid framework</title>
    <link rel="icon" href="@asset('rapid.png')"/>
    <style>
        .card,
        .envelope {
            background: #fff;
            box-shadow: -1px 8px 3px 0 rgba(0, 0, 0, 0.6);
            height: 21em;
            padding: 2em;
            position: relative;
            width: 15em;
        }

        .envelope {
            background: #cb231c;
            box-shadow: 0 3px 2px rgba(0, 0, 0, 0.8);
            margin: -2em -2em;
            position: absolute;
            transform: rotate(-8deg);
            z-index: -1;
        }

        h1 {
            text-align: center;
            font-size: 2.2em;
            font-weight: 900;
            margin: 0;
            text-transform: uppercase;
            width: 90%;
        }

        em {
            color: #cc2119;
            font-style: normal;
        }

        .heart {
            bottom: 4em;
            position: absolute;
            right: 4em;
        }

        .heart::before,
        .heart::after {
            background-color: #cd231b;
            border-radius: 50px 50px 0 0;
            content: "";
            height: 25px;
            left: 15px;
            position: absolute;
            transform: rotate(-45deg);
            transform-origin: 0 100%;
            width: 15px;
        }

        .heart::after {
            left: 0;
            transform: rotate(45deg);
            transform-origin: 100% 100%;
        }

        html, body {
            height: 100%;
            margin: 0;
        }

        body {
            align-items: center;
            background-color: #eee;
            display: flex;
            font: 16px/1.05 "Montserrat", sans-serif;
        }

        code {
            position: absolute;
            bottom: 15px;
            left: 15px;
            font-size: 0.8em;
        }

        .container {
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="container">

    <div class="card">
        <div class="envelope"></div>
        <h1>
            <em>⚡️ Rapid ⚡️</em> Framework
        </h1>
        <hr>

        <em><b>Creator</b></em>: Lazizbek Ergashev
        <code>{{ $_SERVER['SERVER_SOFTWARE'] }} ({{ $year }})</code>
        <div class="heart"></div>
    </div>
</div>
</body>
</html>