<?php

namespace Core\Router;

class Router
{
    public static function get($url, $action)
    {
        app()->router->addRoute('get', $url, $action);
    }
}