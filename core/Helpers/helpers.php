<?php

use Core\Application\Application;
use Core\Request\Request;

function app(): Application
{
    return Application::$app;
}

function request(): Request
{
    return Application::$app->request;
}

function assets($path = ''): string
{
    return HOST . 'assets/' . trim($path, '/');
}

function env(string $var, mixed $default = '')
{
    return $_ENV[$var] ?? $default;
}

function php(string $code, bool $isEcho = true): string
{
    return $isEcho
        ? sprintf("<?php echo e(%s) ?>", $code)
        : sprintf("<?php %s ?>", $code);
}

function e(string $value, bool $doubleEncode = true)
{
    return htmlspecialchars($value, ENT_QUOTES, 'UTF-8', $doubleEncode);
}